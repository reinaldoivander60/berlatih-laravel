<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function datang(Request $request){
        $nama_depan = $request['fname'];
        $nama_belakang = $request['lname'];

        return view('halaman.datang', compact('nama_depan','nama_belakang'));
    }
}
