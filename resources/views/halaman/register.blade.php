<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
        <form action="/datang" method="post">
            @csrf
            <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname"><br><br>
            
            <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname">
            <p>Gender:</p>       
            <input type="radio" id="male" name="fav_language" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="fav_language" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="fav_language" value="other">
            <label for="other">Other</label><br>
            <label for="country">Nationality:</label><br><br>
            <select id="country" name="Negara">
                <option value="Indonesia"selected>Indonesia</option>
                <option value="Malaysia">Malaysia </option>
                <option value="Singapore">Singapore</option>
                <option value="Laos">Laos</option>
            </select>
            <p>Language Spoken:</p>        
            <input type="checkbox" id="Indo" name="Indo" value="bindo">
            <label for="Indo">Bahasa Indonesia</label><br>
            <input type="checkbox" id="ing" name="ing" value="english">
            <label for="ing">English</label><br>
            <input type="checkbox" id="other" name="other" value="other">
            <label for="other">Other</label><br><br>
            <p>Bio:</p>        
            <textarea name="message" rows="10" cols="30"></textarea>
            <br><br>

            <input type="submit" value="SignUp">
            
        </form>
    
</body>
</html>